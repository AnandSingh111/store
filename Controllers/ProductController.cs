﻿using Store.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Store.Controllers
{
    public class ProductController : Controller
    {
        public ActionResult Index()
        {
            // Add action logic here
            throw new NotImplementedException();
        }
        public ActionResult Details(int Id)
        {
            if (Id < 1)
                return RedirectToAction("Index");
            var product = new Product(Id, "Laptop");
            return View("Details", product);
        }

        public ActionResult Test1(int Id)
        {
            if (Id < 1)
                return RedirectToAction("Index");
            var product = new Product(Id, "Laptop");
            return View("Details", product);
        }
    }

    
}