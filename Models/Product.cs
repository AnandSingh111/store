﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Store.Models
{
    public class Product
    {
        public int id;
        public string Name;

        public Product(int id, string Name)
        {
            this.id = id;
            this.Name = Name;
        }
    }
}